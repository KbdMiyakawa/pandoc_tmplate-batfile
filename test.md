# テストパターン

## テストパターン

### テストパターン

* 箇条書き
* 箇条書き
* 箇条書き
* 箇条書き
* 箇条書き


1. 箇条書き
    - サンふじ
    - 紅玉
2. 箇条書き
3. 箇条書き


* フルーツ
  + りんご
       - サンふじ
       - 紅玉
  + なし
  + もも
* 野菜
    + ブロッコリー
    + セロリ



```command.com
pandoc foo.md
	-o foo.tex
	-listings
	--from markdown-auto_identifiers
	--template KBplatex.tmplate -V uplatex
```

```
%!TEX program = uplatex
\documentclass[uplatex,dvipdfmx]{jsarticle}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{longtable}
\usepackage{booktabs}
{aaaaa}


\begin{document}

\end{document}
```
