@echo off
if "%1"=="" goto :eof
set path=X:\TeX_Protection\texlive\2017\bin\win32;%path%
set startpath=%~d0%~p0
set tmplate=%startpath%KBplatex.tmplate
set infile=%1
set outfile=%~n1
set inputpath=%~d1%~p1
pandoc %inputpath%\%infile% -o %inputpath%\%outfile%.tex --from markdown-auto_identifiers  --listings --template %tmplate%  -V uplatex -V toc -V hyperref 
uplatex %inputpath%\%outfile%.tex
dvipdfmx %inputpath%\%outfile%.dvi
del %inputpath%\%outfile%.log
del %inputpath%\%outfile%.aux
del %inputpath%\%outfile%.dvi
:eof

