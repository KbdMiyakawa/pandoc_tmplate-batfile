# これはなに？


pandocでMarkdownを(u)platexでPDF化するもの．

pandocがないと無力．

使い方：

```
md2latex test.md
```

たったこれだけでtest.mdがuplatexを通してPDFになる．

* KBplatex.tmplateは，md2latex.batと同じ場所に置く．（必ず！）
* plistings.styが必要（ここにある）
* pandocのオプションはbatファイルを直接編集（option化はどうしよう？）
* (u)platex文書にパッケージとか追加したければ，KBplatex.tmplateをいじる．
（やり方はたぶんみればわかる）


