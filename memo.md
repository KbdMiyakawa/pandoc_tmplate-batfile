# pandocメモ

pandocでMarkdownからLaTeXへ変換すると``\hypertarget``が妙な具合に絡んでソースが見づらくなる．
これが邪魔なので発生させない方法を探した．

```command.com
--from markdown-auto_identifiers
```
を指定すると，発生しなかった．


コマンドラインはfoo.(tex|md)の場合（実際は1行）
```command.com
pandoc foo.md
	-o foo.tex
	--from markdown-auto_identifiers
```
となる．

* ``-o foo.tex``：書き出しtexファイル
* `--from markdown-auto_identifiers`：`\hypertarge`を生成しない．


# 自家製pandoc用のMarkdows → (u)pLaTeX変換テンプレート

KBplatex.tmplateというテンプレートを作った．

以下全貌：

**KBplatex.tmplate**
```
$if(uplatex)$
%!TEX program = uplatex
\documentclass[uplatex,dvipdfmx]{jsarticle}
$else$
%!TEX program = platex
\documentclass[10pt,dvipdfmx]{jsarticle}
$endif$
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{fancyvrb}

$if(listings)$
\usepackage{listings}
\newcommand{\passthrough}[1]{#1}
\usepackage{plistings}
\definecolor{KeyWordColor}{rgb}{0,0,1}
\definecolor{IdentifierColor}{rgb}{0,0,0}
\definecolor{CommentsColor}{gray}{0.6}
\definecolor{StringColor}{rgb}{0,0.5,0}
\definecolor{BackColor}{gray}{.9}
\lstset{% General setup: listings
%language=tex,
language={[LaTeX]TeX},
morekeywords={NewDocumentCommand,RenewDocumentCommand},
breaklines=true,frame=leftline
    identifierstyle=\color{IdentifierColor}, %??
    keywordstyle=\color{KeyWordColor}, %
    stringstyle=\color{StringColor}, %
    commentstyle=\color{CommentsColor}, %
    backgroundcolor=\color{BackColor}, %
%    commentstyle=\color{gray!70},
    columns=flexible, %
    tabsize=2,
%    columns=fixed,
    showstringspaces=false,
    showtabs=false,
    showspaces=false,
    numberblanklines=false
%    keepspaces,
%    style=tcblatex,
    numbers=left,
    numberstyle=\tiny\sffamily,
    basicstyle=\small\ttfamily,%\bfseries\color{black}
    frame=single, %
    }

\usepackage[all]{xy}
$else$
\newcommand{\VerbBar}{|}
\newcommand{\VERB}{\Verb[commandchars=\\\{\}]}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
% Add ',fontsize=\small' for more characters per line
\usepackage{framed}
\definecolor{shadecolor}{RGB}{248,248,248}
\newenvironment{Shaded}{\begin{snugshade}}{\end{snugshade}}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{0.94,0.16,0.16}{#1}}
\newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.77,0.63,0.00}{#1}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\BuiltInTok}[1]{#1}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{#1}}}
\newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{#1}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{#1}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\ErrorTok}[1]{\textcolor[rgb]{0.64,0.00,0.00}{\textbf{#1}}}
\newcommand{\ExtensionTok}[1]{#1}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\ImportTok}[1]{#1}
\newcommand{\InformationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{#1}}}
\newcommand{\NormalTok}[1]{#1}
\newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.81,0.36,0.00}{\textbf{#1}}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{#1}}
\newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{#1}}}
\newcommand{\RegionMarkerTok}[1]{#1}
\newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\VariableTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\WarningTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
$endif$


$if(hyperref)$
\usepackage{hyperref}
\usepackage{pxjahyper}
\hypersetup{
 bookmarksnumbered=true,
 colorlinks=true,
 urlcolor=blue,
 setpagesize=false,
 pdftitle={},
 pdfauthor={},
 pdfsubject={},
 pdfkeywords={TeX; dvipdfmx; hyperref; color;}}
 \def\tightlist{}
$endif$


\begin{document}
\VerbatimFootnotes
$if(toc)$
\tableofcontents
$endif$

$body$

\end{document}
 @pgbrandolin
 ```

テンプレートは`--template`で指定する．

foo.md，foo.texとした場合，


```command.com
pandoc foo.md
	-o foo.tex
	-listings
	--from markdown-auto_identifiers
	--template platex.tmplate -V uplatex
```

* ``-o foo.tex``：書き出しtexファイル
* ``-listings``：listingパッケージ使用
* `--from markdown-auto_identifiers`：`\hypertarge`を生成しない．
* `--template platex.tmplate`：(latex用) テンプレート（下記に例）の指定
* `-V uplatex`：テンプレート内のオプション


## KBplatex.tmplateの内容

### オプション

```
-listings  % verbatimにlistingsを使う．
-V uplatex   % uplatex用のヘッダになる．指定なしはplatex．
-V toc       % 目次
-V hyperref  % hyperrefパッケージを使う

```

概略

```
$if(uplatex)$
	%!TEX program = uplatex
	\documentclass[12pt,uplatex,dvipdfmx]{jsbook}
$else$
	%!TEX program = platex
	\documentclass[12pt]{jsbook}
$endif$

\usepackage{graphicx}
\usepackage{xcolor}

$if(listings)$
	\usepackage{listings}
$else$
	\usepackage{fancyvrb}
$endif$

\begin{document}
$if(toc)$
	\tableofcontents
$endif$
$body$
\end{document}
```

全体的に書式は簡易で，`$if(XXXX)$`はテンプレートの`-V`によるオプション文字指定．
`$endif$`が終端．`$else$`が使える．

## 起動用バッチファイル

**md2latex.bat**

```
@echo off
if "%1"=="" goto :eof
set path=X:\TeX_Protection\texlive\2017\bin\win32;%path%
set startpath=%~d0%~p0
set tmplate=%startpath%KBplatex.tmplate
set infile=%1
set outfile=%~n1
set inputpath=%~d1%~p1
pandoc %inputpath%\%infile% -o %inputpath%\%outfile%.tex --from markdown-auto_identifiers  --listings --template %tmplate%  -V uplatex -V toc -V hyperref 
uplatex %inputpath%\%outfile%.tex
dvipdfmx %inputpath%\%outfile%.dvi

:eof
```

